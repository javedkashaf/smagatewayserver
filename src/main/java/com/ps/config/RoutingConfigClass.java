package com.ps.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoutingConfigClass {
	
	@Bean
	public RouteLocator configureRoutesData(RouteLocatorBuilder builder) {
		return builder.routes()
				.route("smaCompanyServiceId", r->r.path("/company/**").uri("lb://COMPANY-SERVICE"))
				.route("smaCustomerServiceId", r->r.path("/customer/**").uri("lb://CUSTOMER-SERVICE"))
				.build();
	}

}
