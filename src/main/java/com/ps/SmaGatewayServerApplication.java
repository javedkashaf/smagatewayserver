package com.ps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SmaGatewayServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmaGatewayServerApplication.class, args);
	}

}
